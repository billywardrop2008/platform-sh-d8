<?php

namespace Drupal\Tests\hello\Unit;

use Drupal\hello\Hello;
use Drupal\Tests\UnitTestCase;

/**
 * Hello unit test. I have modified this
 */
class HelloTest extends UnitTestCase {

  protected $unit;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    $this->unit = new Hello();
  }

  /**
   * Test hello-saying method.
   */
  public function testHello() {
    $this->assertEquals('Hey this is a great ', $this->unit->sayHello('world'), 'Concatenation done right.');
  }

}
